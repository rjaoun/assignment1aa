﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1AA.aspx.cs" Inherits="Assignment1AA.Assignment1AA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>
                <legend style="text-align: center; font-weight: bold; font-size: 20px;">Soccer Team under 16</legend>


<!-- Label with TextBox for First Name with RequiredFieldValidator to make sure user entered the first name -->
                    <asp:Label ID="FirstName" runat="server">First Name: </asp:Label>
                    <asp:TextBox ID="fname" runat="server" placeholder="first name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="fname" ErrorMessage="Please Type your First Name" />
                    <br />
 <!-- Label with TextBox for Last Name with RequiredFieldValidator to make sure user entered the last name -->               
                    <asp:Label ID="lname" runat="server">Last Name: </asp:Label>
                    <asp:TextBox ID="lastname" runat="server" placeholder="last name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        ControlToValidate="lastname" ErrorMessage="Please Type your Last Name" />
                    <br />

 <!-- Label with TextBox for School Name with RequiredFieldValidator to make sure user entered the school name -->            
                    <asp:Label ID="schoolname" runat="server">School Name: </asp:Label>
                    <asp:TextBox ID="SName" runat="server" placeholder="school name"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                        ControlToValidate="SName" ErrorMessage="Please Type your School Name" />
                    <br />

 <!-- Label with TextBox for password-->
                    <asp:Label ID="Password" runat="server">Password: </asp:Label>
                    <asp:TextBox ID="pass" runat="server" TextMode="Password"></asp:TextBox>
                    <br />

 <!-- Label with TextBox for confirming the password match using CompareValidator -->
                    <asp:Label ID="CPassword" runat="server">Confirm Password: </asp:Label>
                    <asp:TextBox ID="cpass" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ErrorMessage="Password and confirm Password does not match."
                        ControlToValidate="cpass" runat="server"></asp:CompareValidator>
                    <br />
 <!-- Label with TextBox for Phone number with RegularExpressionValidator to make sure
      the phone number format is (000)000 - 0000  -->            
                    <asp:Label runat="server">Phone Number: </asp:Label>
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator 
                        ID="regPhone" 
                        runat="server" 
                        ControlToValidate="txtPhone" 
                        ValidationExpression="^(\(?\s*\d{3}\s*[\)\-\.]?\s*)?[2-9]\d{2}\s*[\-\.]\s*\d{4}$"
                        Text="Enter a valid phone number" />
                    <br />

 <!-- Label with two RadioButtons to choose the age -->            
                    <asp:Label runat="server">Age: </asp:Label>
                    <asp:RadioButton ID="Under16" runat="server" Text="Under 16" GroupName="Age"/>
                    <asp:RadioButton ID="Over16" runat="server" Text="Over 16" GroupName="Age"/>
                    <br />
    
 <!-- Label with three RadioButtons to choose the gender -->                
                    <label runat="server">Gender: </label>
                    <asp:RadioButton ID="Male" runat="server" Text="Male" GroupName="gender" />
                    <asp:RadioButton ID="Female" runat="server" Text="Female" GroupName="gender" />
                    <asp:RadioButton ID="Other" runat="server" Text="Other" GroupName="gender" />
                    <br />

 <!-- Label with Three CheckBoxes to choose the best time for practing -->            
                    <asp:Label runat="server">Practice Time: </asp:Label>
                    <asp:CheckBox ID="Morning" runat="server" Text="Morning"/>
                    <asp:CheckBox ID="Evening" runat="server" Text="Evening"/>
                    <asp:CheckBox ID="Afternoon" runat="server" Text="Afternoon"/>
                    <br />

<!-- Label with 4 CheckBoxes to choose types of position the user plays -->
                    <asp:Label runat="server">Position: </asp:Label>
                    <asp:CheckBox ID="GoalKeeper" runat="server" Text="Goal Keeper" />
                    <asp:CheckBox ID="Defender" runat="server" Text="Defender" />
                    <asp:CheckBox ID="Attacker" runat="server" Text="Attacker" />
                    <asp:CheckBox ID="Midfielder" runat="server" Text="Midfielder" />
                    <br />

 <!-- Label with DropDownList to choose player experience -->
                    <asp:Label runat="server">Experience: </asp:Label>
                    <asp:DropDownList ID="DropDownList1" runat="server" >  
                    <asp:ListItem Value="">Please Select</asp:ListItem>  
                    <asp:ListItem>Beginner </asp:ListItem>              
                    <asp:ListItem>Intermediate </asp:ListItem>  
                    <asp:ListItem>Advanced</asp:ListItem>
                    <asp:ListItem>Professional</asp:ListItem>                
                    </asp:DropDownList>  
                    <br />

<!-- Label with 2 RadioButtons for Allergies checking -->
                    <asp:Label runat="server">Allergies: </asp:Label>
                    <asp:RadioButton ID="Yes" runat="server" Text="Yes" GroupName="Allergies" />
                    <asp:RadioButton ID="No" runat="server" Text="No" GroupName="Allergies" />
                    <br />

                    <asp:Label runat="server">E-mail: </asp:Label>
                    <asp:TextBox runat="server" TextMode="Email"></asp:TextBox>
                <br />

<!-- Submit button -->                
                    <asp:Button ID="Submit" runat="server" Text="Submit" BorderStyle="Solid" ToolTip="Submit"/>
                    
                    <asp:ValidationSummary ForeColor="Red" runat="server" ID="validationSummary" /> 
                 </fieldset>   
            </div>
            

    </form>
</body>
</html>
